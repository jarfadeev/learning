import re

a = '1.199.4.96 -  - [29/Jun/2017:04:03:07 +0300] "GET /api/v2/banner/12396003/statistic/?date_from=2017-06-29&date_to=2017-06-29 HTTP/1.1" 200 117 "-" "Lynx/2.8.8dev.9 libwww-FM/2.14 SSL-MM/1.4.1 GNUTLS/2.10.5" "-" "1498698187-3800516057-4709-9932399" "e5ed182650494fc006c7c" 0.075\n'
pattern = re.compile(r'^(\S+) +(\S+) +(\S+) +\[(.+)\] +\"(\S+)\s?(\S+)?\s?(\S+)?" (\d+|-) (\d+|-) +\"(.+)\" +\"(.+)\" +\"(.+)\" +\"(.+)\" +\"(.+)\" +(\d+\.?\d+)$')
res = re.match(pattern, a)
print(res.groups())