import marshal
import types
import codecs
import time
import struct


# ENCODE return bytes!!!
# DECODE returns str!!!


# fd = open('__pycache__/cached_vessels.cpython-36.pyc', 'rb')
# magic = fd.read(4)  # python version specific magic num
# date = fd.read(4)  # python version specific magic num
# wtf = fd.read(4)    # wtf
# print(magic)
# print(date)
# print(wtf)
# modtime = time.asctime(time.localtime(struct.unpack('I', date)[0]))
# # print(codecs.decode(magic, 'hex'))
# print(codecs.encode(magic, 'hex'))
# print(codecs.encode(date, 'hex'))
# print(codecs.encode(wtf, 'hex'))
# print(modtime)
# code_object = marshal.load(fd)
# print(code_object.co_consts)
#
#
# def inspect_code_object(co_obj, indent=''):
#     print(indent, "%s(lineno:%d)" % (co_obj.co_name, co_obj.co_firstlineno))
#     for c in co_obj.co_consts:
#         if isinstance(c, types.CodeType):
#             inspect_code_object(c, indent + '  ')
#
#
# inspect_code_object(code_object)
# fd.close()

import dis



def f(a, b):
    a, b = 1, 0
    if a or b:
        print("Hello", a)


co = dis.Bytecode(f)
dis.dis(co.codeobj)
for b in co.codeobj.co_code:
    print(b)
print(co.codeobj.co_code)
print(codecs.encode(co.codeobj.co_code, 'hex'))
print(dis.opmap)


