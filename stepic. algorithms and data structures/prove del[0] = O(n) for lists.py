import time
from collections import deque

for elements in (10**5, 5*10**5, 10**6):
    lst = list()
    queue = deque()

    for i in range(elements):
        lst.append(0)
        queue.append(0)

    start_time = time.time()
    while lst:
        del lst[0]
    print(f'del from list: {time.time() - start_time}')

    start_time = time.time()
    while queue:
        queue.popleft()
    print(f'del from queue: {time.time() - start_time}')

