import time
start = time.time()
import sys


def run(n, tree):
    heights = []
    checked_indexes = {}
    for item_index, parent_index in enumerate(tree):
        height = 1
        while parent_index != -1:
            height += 1
            parent_index = tree[parent_index]
            calc_height = checked_indexes.get(parent_index, 0)
            if calc_height:
                height += calc_height
                break
        checked_indexes[item_index] = height
        heights.append(height)
    return max(heights)


# n = sys.stdin.readline().rstrip('\n')
# tree = list(map(int, sys.stdin.readline().rstrip('\n').split(' ')))
n = 1000000
tree = list(range(-1, n))
print(run(n, tree))
end = time.time()
print(end-start)