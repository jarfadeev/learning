# стек служит для складывания туда чисел до тех пор, пока не наберем m
# как только наберем m, начинаем формировать из этого стека стек максимумов.
# таким образом мы как бы движемся от m-го элемента массива налево и считаем максимум всех элементов справа текущего
# результат - максимум из элементов справа от текущего в сформированном стеке и максимум в формирующемся
import random
from collections import deque
n = 10**6
arr = [random.randrange(10) for i in range(n)]
m = 10000
in_stack = []
out_stack = []
out_max_stack = []
res = []
in_max = 0
queue = deque()
for val in arr:
    # O(n) version
    if len(in_stack) < m:
        in_stack.append(val)
        in_max = max(val, in_max)
    if len(in_stack) == m:
        while in_stack:
            new_out_val = in_stack.pop()
            out_stack.append(new_out_val)
            try:
                out_max_stack.append(max(new_out_val, out_max_stack[-1]))
            except IndexError:
                out_max_stack.append(new_out_val)
        in_max = 0
    if out_stack:
        out_stack.pop()
        res.append(str(max(in_max, out_max_stack.pop())))

    # O(n*m) version
    # queue.appendleft(val)
    # if len(queue) > m:
    #     queue.pop()
    # res.append(str(max(queue)))

print(' '.join(res))

