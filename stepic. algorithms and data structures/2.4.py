import sys
n_reqs = int(sys.stdin.readline())
stack = []
max_stack = []
for i in range(n_reqs):
    command = sys.stdin.readline().split()
    if command[0] == 'push':
        val = int(command[1])
        stack.append(val)
        try:
            max_value = max(max_stack[-1], val)
        except IndexError:
            max_value = val
        max_stack.append(max_value)
    elif command[0] == 'pop':
        stack.pop()
        max_stack.pop()
    elif command[0] == 'max':
        print(max_stack[-1])
