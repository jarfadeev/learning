# SOLUTION 1
# from collections import deque
#
#
# buff_size, n = map(int, input().split())
# packs = deque()
# queue = deque()
# results = {}
# for pack_number in range(n):
#     arrival, duration = map(int, input().split())
#     packs.appendleft([pack_number, arrival, duration])
#
#
# def add_packs(queue, packs, current_time, results):
#     while packs and packs[-1][1] <= current_time:
#         new_pack = packs.pop()
#         if not queue and new_pack[2] == 0:
#             results[new_pack[0]] = current_time
#         else:
#             if len(queue) >= buff_size:
#                 results[new_pack[0]] = -1
#             else:
#                 queue.appendleft(new_pack)
#
#
# current_time = 0
# while packs:
#     add_packs(queue, packs, current_time, results)
#     while queue:
#         pack_number, arrival, duration = queue[-1]
#         results[pack_number] = current_time
#         current_time += duration
#         add_packs(queue, packs, max(current_time-1, 0), results)
#         queue.pop()
#         add_packs(queue, packs, current_time, results)
#     else:
#         current_time += 1
#
#
# for i in range(len(results)):
#     print(results[i])

# SOLUTION 2 smallest i can make
# even smaller is without deque, but that defeats the purpose of queues

from collections import deque


buff_size, n = map(int, input().split())
queue = deque()

busy_until = 0
for i in range(n):
    arrival, duration = map(int, input().split())
    while queue and queue[-1] <= arrival:
        queue.pop()
    if len(queue) < buff_size:
        print(max(arrival, busy_until))
        busy_until += duration
        queue.appendleft(busy_until)
    else:
        print(-1)
