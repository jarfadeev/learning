import sys


def run(brackets_seq):
    stack = []
    opening_brackets = ('(', '[', '{')
    closing_brackets = (')', ']', '}')
    bracket_pairs = {closing_brackets[i]: ob for i, ob in enumerate(opening_brackets)}
    for i, br in enumerate(brackets_seq, start=1):
        if br in opening_brackets:
            stack.append((br, i))
        elif br in closing_brackets and (not stack or bracket_pairs[br] != stack.pop()[0]):
            return i
    return 'Success' if not stack else stack.pop()[1]


brackets_seq = sys.stdin.readline().rstrip('\n')
print(run(brackets_seq))
