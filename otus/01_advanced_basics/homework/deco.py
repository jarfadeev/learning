#!/usr/bin/env python
# -*- coding: utf-8 -*-

from functools import update_wrapper, wraps


def disable(f):
    '''
    Disable a decorator by re-assigning the decorator's name
    to this function. For example, to turn off memoization:

    >>> memo = disable

    '''
    return f


def decorator(dec):
    '''
    Decorate a decorator so that it inherits the docstrings
    and stuff from the function it's decorating.
    '''
    def wrapper(f):
        return update_wrapper(dec(f), f)
    return wrapper


def countcalls(f):
    '''Decorator that counts calls made to the function decorated.'''
    @wraps(f)
    def wrapper(*args, **kwargs):
        res = f(*args, **kwargs)
        wrapper.calls += 1
        return res
    wrapper.calls = 0
    return wrapper


def memo(f):
    '''
    Memoize a function so that it caches all return values for
    faster future lookups.
    '''
    @wraps(f)
    def wrapper(*args, **kwargs):
        args_stringified = ','.join(map(str, args))
        res = wrapper.cache.get(args_stringified, None)
        if not res:
            res = f(*args, **kwargs)
            wrapper.cache[args_stringified] = res
        update_wrapper(wrapper, f)
        return res
    wrapper.cache = {}
    return wrapper


def n_ary(f):
    '''
    Given binary function f(x, y), return an n_ary function such
    that f(x, y, z) = f(x, f(y,z)), etc. Also allow f(x) = x.
    '''
    @wraps(f)
    def wrapper(*args, **kwargs):
        res = args[0]
        if len(args) >= 2:
            for arg in args[1:]:
                res = f(res, arg)
        return res
    return wrapper


def trace(annotation):
    '''Trace calls made to function decorated.
    @trace("____")
    def fib(n):
        ....
    >>> fib(3)
     --> fib(3)
    ____ --> fib(2)
    ________ --> fib(1)
    ________ <-- fib(1) == 1
    ________ --> fib(0)
    ________ <-- fib(0) == 1
    ____ <-- fib(2) == 2
    ____ --> fib(1)
    ____ <-- fib(1) == 1
     <-- fib(3) == 3
    '''
    def dec(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            func_info = f"{f.__name__}({', '.join(map(str, args))})"
            print(f"{annotation * wrapper.level} --> {func_info}")
            wrapper.level += 1
            result = f(*args)
            wrapper.level -= 1
            print(f'{annotation * wrapper.level} <-- {func_info} == {result}')
            return result
        wrapper.level = 0
        return wrapper
    return dec


@memo
@countcalls
@n_ary
def foo(a, b):
    return a + b


@countcalls
@memo
@n_ary
def bar(a, b):
    return a * b


@countcalls
@trace("####")
@memo
def fib(n):
    """Some doc"""
    return 1 if n <= 1 else fib(n-1) + fib(n-2)


def main():
    print(foo(4, 3))
    print(foo(4, 3, 2))
    print(foo(4, 3))
    print("foo was called", foo.calls, "times")

    print(bar(4, 3))
    print(bar(4, 3, 2))
    print(bar(4, 3, 2, 1))
    print("bar was called", bar.calls, "times")

    print(fib.__doc__)
    fib(3)
    print(fib.calls, 'calls made')


if __name__ == '__main__':
    main()
