#!/usr/bin/env python
# -*- coding: utf-8 -*-

# -----------------
# Реализуйте функцию best_hand, которая принимает на вход
# покерную "руку" (hand) из 7ми карт и возвращает лучшую
# (относительно значения, возвращаемого hand_rank)
# "руку" из 5ти карт. У каждой карты есть масть(suit) и
# ранг(rank)
# Масти: трефы(clubs, C), пики(spades, S), червы(hearts, H), бубны(diamonds, D)
# Ранги: 2, 3, 4, 5, 6, 7, 8, 9, 10 (ten, T), валет (jack, J), дама (queen, Q), король (king, K), туз (ace, A)
# Например: AS - туз пик (ace of spades), TH - дестяка черв (ten of hearts), 3C - тройка треф (three of clubs)

# Задание со *
# Реализуйте функцию best_wild_hand, которая принимает на вход
# покерную "руку" (hand) из 7ми карт и возвращает лучшую
# (относительно значения, возвращаемого hand_rank)
# "руку" из 5ти карт. Кроме прочего в данном варианте "рука"
# может включать джокера. Джокеры могут заменить карту любой
# масти и ранга того же цвета, в колоде два джокерва.
# Черный джокер '?B' может быть использован в качестве треф
# или пик любого ранга, красный джокер '?R' - в качестве черв и бубен
# любого ранга.

# Одна функция уже реализована, сигнатуры и описания других даны.
# Вам наверняка пригодится itertoolsю
# Можно свободно определять свои функции и т.п.
# -----------------
import itertools


def hand_rank(hand):
    """Возвращает значение определяющее ранг 'руки'"""
    ranks = card_ranks(hand)
    if straight(ranks) and flush(hand):
        return (8, max(ranks))
    elif kind(4, ranks):
        return (7, kind(4, ranks), kind(1, ranks))
    elif kind(3, ranks) and kind(2, ranks):
        return (6, kind(3, ranks), kind(2, ranks))
    elif flush(hand):
        return (5, ranks)
    elif straight(ranks):
        return (4, max(ranks))
    elif kind(3, ranks):
        return (3, kind(3, ranks), ranks)
    elif two_pair(ranks):
        return (2, two_pair(ranks), ranks)
    elif kind(2, ranks):
        return (1, kind(2, ranks), ranks)
    else:
        return (0, ranks)


def card_ranks(hand):
    """Возвращает список рангов (его числовой эквивалент),
    отсортированный от большего к меньшему"""
    dct = {
        'T': 10,
        'J': 11,
        'Q': 12,
        'K': 13,
        'A': 14
    }
    return sorted([dct.get(card[0], None) or int(card[0]) for card in hand], reverse=True)


def flush(hand):
    """Возвращает True, если все карты одной масти"""
    suit = hand[-1][-1]
    for card in hand:
        if card[-1] != suit:
            return False
    return True


def straight(ranks):
    """Возвращает True, если отсортированные ранги формируют последовательность 5ти,
    где у 5ти карт ранги идут по порядку (стрит)"""
    for i in range(len(ranks)-1):
        if ranks[i] - ranks[i+1] != 1:
            return False
    return True


def kind(n, ranks):
    """Возвращает первый ранг, который n раз встречается в данной руке.
    Возвращает None, если ничего не найдено"""
    for rank, rank_list in itertools.groupby(ranks):
        rank_list = list(rank_list)
        if len(rank_list) == n:
            return rank
    return None


def two_pair(ranks):
    """Если есть две пары, то возврщает два соответствующих ранга,
    иначе возвращает None"""
    rks = []
    for rank, rank_list in itertools.groupby(ranks):
        rank_list = list(rank_list)
        if len(rank_list) == 2:
            rks.append(rank)
    return rks or None


def best_5_hand(possible_5_hands):
    best = ''
    best_rank = [0]
    for hand in possible_5_hands:
        rank = hand_rank(hand)
        for i, val in enumerate(rank):
            if val > best_rank[i]:
                best = hand
                best_rank = rank
            elif val < best_rank[i]:
                break
    return best


def best_hand(hand):
    """Из "руки" в 7 карт возвращает лучшую "руку" в 5 карт """
    return best_wild_hand(hand)


def best_wild_hand(hand):
    """best_hand но с джокерами"""
    no_joker_hand = []
    jokers = []
    for card in hand:
        if card in ('?B', '?R'):
            jokers.append(card)
        else:
            no_joker_hand.append(card)
    ranks = list(range(2, 10)) + ['T', 'J', 'Q', 'K', 'A']
    suits = {'B': ['C', 'S'], 'R': ['H', 'D']}
    possible_joker_cards = {
        '?B': [str(r)+s for r in ranks for s in suits['B']],
        '?R': [str(r)+s for r in ranks for s in suits['R']]
    }
    all_joker_combinations = itertools.product(*[possible_joker_cards[joker] for joker in jokers])
    possible_5_hands = []
    for joker_cards in all_joker_combinations:
        if not set(joker_cards) & set(no_joker_hand):
            hand = no_joker_hand + list(joker_cards)
            possible_5_hands += list(itertools.combinations(hand, 5))

    return best_5_hand(possible_5_hands)


def test_best_hand():
    print("test_best_hand...")
    assert (sorted(best_hand("6C 7C 8C 9C TC 5C JS".split()))
            == ['6C', '7C', '8C', '9C', 'TC'])
    assert (sorted(best_hand("TD TC TH 7C 7D 8C 8S".split()))
            == ['8C', '8S', 'TC', 'TD', 'TH'])
    assert (sorted(best_hand("JD TC TH 7C 7D 7S 7H".split()))
            == ['7C', '7D', '7H', '7S', 'JD'])
    print('OK')


def test_best_wild_hand():
    print("test_best_wild_hand...")
    assert (sorted(best_wild_hand("6C 7C 8C 9C TC 5C ?B".split()))
            == ['7C', '8C', '9C', 'JC', 'TC'])
    assert (sorted(best_wild_hand("TD TC 5H 5C 7C ?R ?B".split()))
            == ['7C', 'TC', 'TD', 'TH', 'TS'])
    assert (sorted(best_wild_hand("JD TC TH 7C 7D 7S 7H".split()))
            == ['7C', '7D', '7H', '7S', 'JD'])
    print('OK')


if __name__ == '__main__':
    test_best_hand()
    test_best_wild_hand()
