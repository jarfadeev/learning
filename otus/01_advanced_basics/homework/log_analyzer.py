import sys
from datetime import datetime
import gzip
import json
from collections import namedtuple, defaultdict
import getopt
from string import Template
import configparser
import pathlib
import logging
import re


"""
This is a python 3.6 project.
Uses pathlib for path and files manipulation and cross platform use
     getopt for command line options parsing
     configparser to parse config files
Default path to config is 'config'
"""

config = {
    "REPORT_SIZE": 1000,
    "REPORT_DIR": "reports",
    "LOG_DIR": "log",
    "PERCENT_ERRORS_THRESHOLD": 10
}


def setup_logger(logger_path):
    logging.basicConfig(filename=logger_path, level=logging.INFO,
                        format='[%(asctime)s] %(levelname).1s %(message)s', datefmt='%Y.%m.%d %H:%M:%S')
    logging.info('Start work')


def parse_config(config_path):
    config = configparser.ConfigParser()
    config.optionxform = str
    config.read(config_path)
    return config


def get_most_recent_log_info(log_dir):
    Log = namedtuple('Log', ['date', 'path'])
    most_recent_log_info = None
    for p in log_dir.iterdir():
        if p.is_file() and p.suffix in ('', '.gz'):
            log_prefix = p.stem.split('.')[0]
            if log_prefix == 'nginx-access-ui':
                datestr = p.stem[-8:]
                date = datetime.strptime(datestr, '%Y%m%d')
                if not most_recent_log_info or date > most_recent_log_info.date:
                    most_recent_log_info = Log(date, p)
    return most_recent_log_info


class LogParser:
    def __init__(self, log):
        self.log = log
        self.parse_errors = 0
        self.parsed_lines = 0
        # the regexp might be not the best - i'm not very good with them
        self.pattern = re.compile(r'^(\S+) +(\S+) +(\S+) +\[(.+)\] +\"(\S+)\s?(\S+)?\s?(\S+)?" (\d+|-) (\d+|-) +'
                                  r'\"(.+)\" +\"(.+)\" +\"(.+)\" +\"(.+)\" +\"(.+)\" +(\d+\.?\d+)$')

    def fast_match(self):
        # this way faster
        for line in self.log:
            line = line.decode('utf-8').split()
            yield line[6], float(line[-1])

    def __iter__(self):
        # this way more consistent with counting errors
        for line in self.log:
            line = line.decode('utf-8')
            res = re.match(self.pattern, line)
            if res:
                self.parsed_lines += 1
                yield res.groups()[5], float(res.groups()[-1])
            else:
                self.parse_errors += 1

    def percent_errors(self):
        return 100 * self.parse_errors / (self.parse_errors + self.parsed_lines)


def get_median(data):
    n = len(data)
    if n % 2 == 1:
        return data[n//2]
    else:
        i = n//2
        return (data[i - 1] + data[i])/2


def main(config):
    # pathlib library is very useful and convenient since project is written with 3.6
    log_dir = pathlib.Path.cwd() / config['LOG_DIR']
    most_recent_log_info = get_most_recent_log_info(log_dir)
    if not most_recent_log_info:
        logging.exception('No log files found.')
        return
    output_file_name = f"report-{most_recent_log_info.date.year}.{most_recent_log_info.date.month:02d}." \
                       f"{most_recent_log_info.date.day}.html"
    output_file_path = pathlib.Path.cwd() / config['REPORT_DIR'] / output_file_name
    if output_file_path.exists():
        logging.exception('Most recent log was already parsed.')
        return

    stats = defaultdict(list)
    report_stat = []
    opener = gzip.open if most_recent_log_info.path.suffix else open
    with opener(most_recent_log_info.path, 'rb') as log:
        log_parser = LogParser(log)
        for url, time in log_parser:
            stats[url] += [time]

    if log_parser.percent_errors() > config['PERCENT_ERRORS_THRESHOLD']:
        logging.exception(f'Percent of errors is higher that threshold: '
                          f'{log_parser.percent_errors()} vs {config["PERCENT_ERRORS_THRESHOLD"]}')
        return
    all_count = 0
    all_time = 0
    for url, times_list in stats.items():
        times_list = sorted(times_list)
        time_sum = sum(times_list)
        all_count += len(times_list)
        all_time += time_sum
        report_stat.append({
            'url': url,
            'count': len(times_list),
            'count_perc': 0,
            'time_sum': round(time_sum, 3),
            'time_perc': 0,
            'time_avg': round(time_sum / len(times_list), 3),
            'time_max': times_list[-1],
            'time_med': round(get_median(times_list), 3)
        })
    report_stat = sorted(report_stat, key=lambda x: x['time_sum'], reverse=True)[:int(config['REPORT_SIZE'])]
    for dct in report_stat:
        dct['count_perc'] = round(100 * dct['count'] / all_count, 3)
        dct['time_perc'] = round(100 * dct['time_sum'] / all_time, 3)
    output_text = Template(pathlib.Path('report.html').read_text(encoding='utf-8'))
    output_text = output_text.safe_substitute(table_json=json.dumps(report_stat))
    output_file_path.write_text(output_text)
    logging.info(f'Report successfully created at {output_file_path}')


if __name__ == "__main__":
    # parse args
    opts, args = getopt.getopt(sys.argv[1:], '', ['config=', ])
    for opt, arg in opts:
        if opt == "--config":
            if arg:
                config_path = pathlib.Path(arg)
            else:
                config_path = pathlib.Path('config')
            if not config_path.exists():
                raise Exception(f'Config file at path "{config_path}" not found')
            file_config = parse_config(config_path)
            for name, val in file_config['DEFAULT'].items():
                config[name] = val
    setup_logger(config.get('LOGFILE', None))
    try:
        main(config)
    except:
        logging.exception('Exception occured')
    logging.info('Stop work.')
